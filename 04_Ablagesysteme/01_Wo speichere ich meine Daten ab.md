# IST-Zustand: Wo speichere ich welche Daten ab?
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  2 Lektionen |
| Ziele | Wo habe ich überall persönliche Daten gespeichert: Sich selber bewusst machen,  an welchen Speicherorten Daten über einen gespeichert sind. Methode: Auflistung von allen Geräten, Services und darauf abgespeicherten Daten. |

Jeder von uns verwendet verschiedene Geräte (Notebook, Smartphone, Tablet) und hat darauf persönliche Daten abgespeichert. In dieser Übung halten Sie fest auf welchem Gerät, Sie welche Informationen gespeichert haben und welche Services Sie verwenden. 
(*Lassen Sie bei Ihrer Auflistung die Daten von Ihrem Arbeitsort weg.*) 

**Tipp**: Lesen Sie zuerst die nachfolgende Aufträge. Eventuell fällt es Ihnen einfacher mit der grafischen Darstellung zu beginnen. 

## 3.1. Vorgehen
 1. Erstellen Sie eine Tabelle mit all ihren persönlichen Mikrocomputern (Notebook, Smartphone, Tablet usw.) mit den wichtigsten Spezifikationen (Individueller Name, Marke, Typ, Speichergrösse, usw...) und deren Verwendungszweck. 
 2. Erstellen Sie eine Tabelle mit allen Apps/Services, die mindestens eine der folgenden Kriterien erfüllen: <br> - (a) dient zur Kommunikation <br> - (b) dient der Kollaboration oder Zusammenarbeit <br> - (c) Beinhaltet personenbezogene Daten, die Sie selbst freigegeben oder eingefügt haben. <br>Notieren Sie zu jeder App und jedem Service, welche persönlichen Daten diese da drauf sind. 
 3. Erstellen Sie eine Tabelle mit allen Speichermedien auf denen Sie Daten abspeichern. (z.B. den Ordner DCIM mit den Fotos und/oder Videos auf Ihrem Smartphone). 

**Halten Sie alles in Ihrem persönlichen Portfolio fest.**

![Beispiel Screenshot Tabelle](images/mydatatable.PNG)
*So könnte der Anfang Ihrer Tabelle aussehen.*


**Tipp:** Lassen Sie Apps, die nur allgemeine Personenbezogene Daten beinhalten (z.B. Name + Adresse für Lieferdienste) weg oder fassen Sie diese zusammen. 

### 3.1.1. Vorlage - Spalten für die Tabelle Apps/Services
 - Gerät (Speicherort)
 - Betriebssystem
 - Applikation
 - Art der Daten
 - Auf zentralem Server gespeichert?
 - User friendly privacy policy?
 - Folgen von Datenverlust (/Zugriffsverlust)<br/><i>Wäre es schlimm/Was wären die Konsequenzen, wenn die Daten unwiederruflich verlohren gehen?</i>
 - Folgen von Datendiebstahl<br/><i>Wäre es schlimm, wenn Freunde oder Fremde zugriff auf diese Daten hätten?</i>
 - Backup vorhanden?<br><i>Ja/Nein</i>
 - Art des Backups
 - Häufigkeit des Backups
 - Massnahmen<br/><i>Welche Massnahmen müssen ergriffen werden, damit die Anforderungen an Datenverlust/diebstahl erfüllt werden?</i>
