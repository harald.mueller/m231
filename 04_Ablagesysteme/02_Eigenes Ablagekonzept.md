# Teil 1: Das eigene Ablagekonzept grafisch darstellen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  4 Lektion |
| Ziele | Übersichtsgrafik des persönliches Ablagekonzept erstellt. |

Mithilfe der Informationen aus der vorhergehenden Übung können Sie nun Ihr Ablagekonzept entwickeln. 

## Vorgehen
 1. Wählen Sie ein vektorbasiertes Zeichnungsprogramm ([draw.io](draw.io), Microsoft Visio, o.ä.) aus.
 2. Stellen Sie nun die Daten aus der vorherigen Übung grafisch dar.

**Lassen Sie allgemeine Services (Onlineshop, Spiele, usw.), die nur Kontaktdaten enthalten, in Ihrer Grafik weg.**

## Beispiel
Die Darstellungsart steht Ihnen frei. Zur Inspiration können diese Grafiken dienen:
![Beispiel 1 grafische Darstellung persönliches Ablagekonzept](images/ablagekonzept_beispiel_1.png)

[Beispiel 2 grafische Darstellung persönliches Ablagekonzept](images/ablagekonzept_beispiel_2.png)

# Teil 2: Das eigene Ablagekonzept analysieren
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Daten nach schutzwürdigkeit einstufen |

In der oben dargestellten Grafik wurden die Datenkategorien und Services teilweise mit Symbolen versehen, die ihre Schutzwürdigkeit definieren. Grundsätzlich gilt es eine Unterscheidung zwischen allgemeinen Daten und schutzwürdigen Personendaten zu machen. Zusätzlich machen wir noch eine Unterscheidung zwischen eigenen und fremden Personendaten. Den während wir das Recht über unsere eigenen Personendaten haben, so müssen wir bei Personendaten von Dritten uns an die Vorgaben des Datenschutzgesetzes halten. Wir definieren die folgende Kategorien:
 - Allgemeine Daten
 - Schützenswerte eigene Personendaten
 - Schützenswerte Personendaten von Dritten

Teilen Sie nun alle Ihre Datenablagen und verwendeten Services in diese drei Kategorien ein. 