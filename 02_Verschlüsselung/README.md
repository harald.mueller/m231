# Verschlüsselung

![https://xkcd.com/2691](images/235px-encryption_2x.png)

*Quelle: https://xkcd.com/2691 *

Eine etabliertes und seit Jahrtausenden bekanntes Mittel um geheime Nachrichten vor Neugierigen zu verstecken ist Verschlüsselung. Aus der Geschichte ist besonders die "Caeasar-Verschlüsselung" bekannt. Sie lautet sich vom römischen Feldherrn *Gaius Julius Caesar* ab, der nach Überleiferung des römischen Schriftstellers Sueton diese Art der geheimen Kommunikation für seine militärische Korrespondenz verwendet hat. (Quelle: [Wikipedia](https://de.wikipedia.org/wiki/Caesar-Verschl%C3%BCsselung#Geschichte))

Heutzutage existieren weit sicherere Verschlüsselungsalgorithmen, die für die digitale Kommunikation und dem Datenschutzbedürfnis eine wichtige und zentrale Rolle spielen.

In diesem Abschnitt schauen wir uns verschiedene Aspekete zum Thema an. 

# Einführungsvideos

Bei der symmetrischen Verschlüsselung wird derselbe Schlüssel zum verschlüsseln und entschlüsseln der Nachricht (Daten) gebraucht. 
Im Video [Symmetrische Verschlüsselung](https://studyflix.de/informatik/symmetrische-verschlusselung-1610) wird dies noch genauer erklärt.

Bei der assymetrischen Verschlüsselung wird ein Schlüsselpaar gebraucht. Um eine Nachricht zu verschlüsseln braucht man den einen der beiden Schlüssel und man kann dann die Nachricht nur mit dem anderen Schlüssel des Paar entschlüsseln.Im Video [Assymetrische Verschlüsselung](https://studyflix.de/informatik/asymmetrische-verschlusselung-1609) wird dies noch genauer erklärt


# Übungen
 - [Cäsar-Verschlüsselung](01_Caesar.md)
 - [PGP](02_PGP.md)
 - [Hash](03_Hash.md)
 - [Veracrypt](04_Veracrypt.md)
 - [Cryptomator](05_Cryptomator.md)


# Links
 - http://project-rainbowcrack.com/table.htm