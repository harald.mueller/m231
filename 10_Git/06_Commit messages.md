# 10. Commit Messages - Introduction: Why good commit messages matter
![https://xkcd.com/1296/](https://imgs.xkcd.com/comics/git_commit.png)

Gut und knackig formulierte *commit messages* sind beim Einsatz von git zentral. Der Blog Artikel [Introduction: Why good commit messages matter](https://chris.beams.io/posts/git-commit/) erklärt die Problematik von schlechten *commit messages* und gibt Tipps für gute *commit messages*. 
