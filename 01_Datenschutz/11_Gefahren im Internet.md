# Gefahren im Internet - Cybercrime

| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einezelarbeit |
| Aufgabenstellung  | Lesen |
| Zeitbudget  |  1 Lektion |
| Ziel | Wichtigte Gefahren bzw. Angriffmethoden im Internet kennen |

Das im Internet Gefahren lauern, ist in unseren Breitengraden gut bekannt. Welche Gefahren auf uns lauern, jedoch weniger. In diesem Abschnitt wird eine Auswahl der gängigsten Gefahren aufgelistet. 

# Übersicht

**Social Engineering** ist ein Angriff, der menschliches Vertrauen und soziales Verhalten ausnutzt.

---

**Pharming** verwendet legitime Dienste, um Benutzer auf eine kompromittierte Website zu leiten.

---


**Phishing** ist eine böswillige Einladung, die als legitim getarnt ist.

---

**Spear-Phishing** ist ein Angriff, der auf eine Gruppe ähnlicher Benutzer abzielt.

---

**Whaling** ist ein Angriff, der auf eine hochkarätige Person abzielt, z. B. einen CEO oder CIO eines Unternehmens.

---


**Smishing** ist ein Angriff, der Textnachrichten (SMS) verwendet.

---

**Vishing** ist ein Angriff, der Sprachanrufe verwendet.

---

Ein **Watering Hole** zielt auf bestimmte Opfer ab, die eine kompromittierte Website besuchen.

---

![Skimming](https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/Extra_stukje_op_beveiligde_pinautomaat_tegen_skimmen.jpg/440px-Extra_stukje_op_beveiligde_pinautomaat_tegen_skimmen.jpg)

**Skimming** betrug mittels gestohlener Kreditkartendaten und PINs. 

---

![Dumpster Diving Fun Gif](media/dumpster-diving.gif)

Beim **Dumpster Diving** werden Informationen aus weggeworfenen Akten, Memos, Organisationsplänen oder Ähnlichem gewonnen. 

---

Beim **Tailgating** geht ein Angreifer, der Zugang zu einem eingeschränkten Bereich sucht, der durch eine unbeaufsichtigte, elektronische Zugangskontrolle gesichert ist, z. B. per RFID-Karte, einfach hinter einer berechtigten Person hinein.

---


# Quellen
 - https://en.wikipedia.org/wiki/Social_engineering_(security)
 - https://en.wikipedia.org/wiki/Credit_card_fraud#Skimming
